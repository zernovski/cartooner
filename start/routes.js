/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Logger = use('Logger')

Route.group(() => {
  Route.get('/', () => ({ greeting: 'Welcome to Dasher API' }))

  Route.post('/login', 'SecurityController.login')
  Route.post('/logout', 'SecurityController.logout')
  Route.get('/profile', 'SecurityController.profile')
    .middleware(['auth'])

  Route.get('/ctx.js', async ({ response, auth }) => {
    let user = null
    try {
      user = JSON.stringify(await auth.getUser())
    } catch (e) {
      if (e.name !== 'InvalidSessionException') {
        Logger.warning('Error while stringifying User', e)
      }
    }


    response.header('Cache-Control', 'private, no-cache, no-store, must-revalidate')
    response.header('Expires', '-1')
    response.header('Pragma', 'no-cache')
    response.header('Content-type', 'application/javascript; charset=UTF-8')
    return `(function(w){w.profile=${user}})(window)`
  })

  Route.resource('users', 'UserController')
    .apiOnly()
    .validator(new Map([
      [['users.store'], ['UserStore']],
      [['users.update'], ['UserStore']],
    ]))
    .middleware(['roles:ADMIN'])

  Route.resource('channels', 'ChannelController')
    .only(['index', 'store', 'show', 'destroy'])
    .validator(new Map([
      [['channels.store'], ['ChannelStore']],
    ]))

  Route.post('/validation/check_email', 'ValidationController.checkEmail')

  Route.get('youtube/channel/:channelId', 'YoutubeController.channelInfo')

  Route.get('videos', 'VideoController.index')
}).prefix('/api/v1')


Route.get('*', ({response}) => response.download('./public/app/index.html'))

