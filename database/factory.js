/*
|--------------------------------------------------------------------------
| Factory
|--------------------------------------------------------------------------
|
| Factories are used to define blueprints for database tables or Lucid
| models. Later you can use these blueprints to seed your database
| with dummy data.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
/** @type {import('../providers/Uuid')} */
const Uuid = use('Uuid')

Factory.blueprint('App/Models/User', async (faker, i, data) => {
  const fullname = faker.name()
  const username = fullname.toLowerCase().replace(' ', '.')

  return {
    id: Uuid.v4(),
    fullname,
    email: data.email || `${username}@reviewer.io`,
    password: 'fake',
    role: data.role || faker.weighted(['USER', 'ADMIN'], [8, 1]),
  }
})
