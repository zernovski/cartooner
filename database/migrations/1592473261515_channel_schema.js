'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ChannelSchema extends Schema {
  up () {
    this.create('channels', (table) => {
      table.uuid('id').unique().notNullable()
      table.string('youtube_id', 24).unique()
      table.string('name', 254)
      table.jsonb('meta')
      table.string('thumbnail', 254)
      table.timestamp('last_fetched_at')
      table.timestamps()
    })

    this.create('videos', (table) => {
      table.uuid('id').unique().notNullable()
      table.string('youtube_id', 11).unique()
      table.string('name', 254)
      table.jsonb('meta')
      table.string('thumbnail', 254)
      table.uuid('channel_id').references('id').inTable('channels')
      table.timestamps()
    })
  }

  down () {
    this.drop('videos')
    this.drop('channels')
  }
}
module.exports = ChannelSchema
