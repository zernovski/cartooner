'use strict'

/*
|--------------------------------------------------------------------------
| DatabaseSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use('Factory')
const User = use('App/Models/User')

class DatabaseSeeder {
  async run () {
    const admin = await User.findBy('email', 'admin@reviewer.io')

    if (!admin) {
      await Factory
        .model('App/Models/User')
        .create({role: 'ADMIN', email: 'admin@reviewer.io'})
    }

    await Factory
      .model('App/Models/User')
      .createMany(10, {role: 'ADMIN'})

    await Factory
      .model('App/Models/User')
      .createMany(50, {role: 'USER'})
  }
}

module.exports = DatabaseSeeder
