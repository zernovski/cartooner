'use strict'
const Youtube = use('Youtube')
const Video = use('App/Models/Video')
const Channel = use('App/Models/Channel')
const Uuid = use('Uuid')
const Logger = use('Logger')

class ChannelDownload {
  // If this getter isn't provided, it will default to 1.
  // Increase this number to increase processing concurrency.
  static get concurrency () {
    return 1
  }

  // This is required. This is a unique key used to identify this job.
  static get key () {
    return 'ChannelDownloadJob'
  }

  // This is where the work is done.
  async handle (data) {
    const { id } = data

    const channel = await Channel.find(id)

    if (!channel) {
      Logger.warning('Unable to find channel with id "%s"', id)
      return
    }

    let result
    const publishedAfter = (channel.last_fetched_at || new Date('01 January 1970 00:00 UTC')).toISOString()
    const order = 'date'

    try {
      Logger.info('Fetching videos for channel "%s" published after %s', channel.name, publishedAfter)

      result = await Youtube.searchVideos('', 50, {
        channelId: channel.youtube_id,
        publishedAfter,
        order,
      })

      Logger.info('Fetched %s video(s) for channel "%s"', result.length, channel.name)
    } catch (e) {
      Logger.error('Unable to fetch videos of channel with id "%s"', channel.youtube_id)
    }

    result.forEach((item) => {
      const video = new Video()

      video.id = Uuid.v4()
      video.youtube_id = item.id
      video.name = item.title
      video.meta = item.raw.snippet
      video.thumbnail = item.thumbnails.medium.url
      video.channel_id = id

      video.save()
    })

    channel.last_fetched_at = new Date()
    channel.save()
  }
}

module.exports = ChannelDownload

