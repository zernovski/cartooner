'use strict'

const { Role } = require('@app/enum')

class ChannelStore {
  async authorize () {
    const me = await this.ctx.auth.getUser()

    if (me.role !== Role.ADMIN) {
      this.ctx.response.unauthorized({message: 'Only admins can manage channels'})
      return false
    }

    return true
  }

  get rules () {
    const channelId = this.ctx.params.id

    const rules = {
      // validation rules
      youtube_url: 'required',
    }

    // if updating existing channel
    if (channelId) {
      // rules.youtube_id = `required|max:24|unique:channels,youtube_id,id,${channelId}`
      rules.youtube_id = `required`
      delete rules.password
    }

    return rules
  }

  get sanitizationRules () {
    return {
      youtube_id: 'trim',
    }
  }

  get validateAll () {
    return true
  }
}

module.exports = ChannelStore
