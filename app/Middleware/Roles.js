const GE = require('@adonisjs/generic-exceptions')

class RolesMiddleware {

  /**
   * @param {object} ctx
   * @param {Function} next
   * @param {array} params
   */
  async handle({ auth }, next, params) {
    const me = await auth.getUser()

    if (me && (params.includes(me.role) || params.includes('user'))) {
      await next()
      return
    }

    if (!me && params.includes('guest')) {
      await next()
      return
    }

    throw new GE.HttpException(`Forbidden`, 403, 'E_FORBIDDEN')
  }
}

module.exports = RolesMiddleware
