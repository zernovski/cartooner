'use strict'

const Task = use('Task')
const kue = use('Kue')
const Job = use('App/Jobs/ChannelDownload')
const Channel = use('App/Models/Channel')
const Logger = use('Logger')

class FetchVideo extends Task {
  static get schedule () {
    return '0 0 */12 * * *'
  }

  async handle () {
    Logger.info('Task FetchVideo handle')

    const yesterday = new Date()

    yesterday.setDate(yesterday.getDate() - 1)

    const result = await Channel
      .query()
      .where('last_fetched_at', '<', yesterday)
      .limit(20)
      .fetch()

    const channels = result.rows

    Logger.info('FetchVideoCron queued %s channels for sync', channels.length)

    channels.forEach((channel) => {
      kue.dispatch(Job.key, {
        id: channel.id,
      })
    })
  }
}

module.exports = FetchVideo
