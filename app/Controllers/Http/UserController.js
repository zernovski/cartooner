'use strict'

const User = use('App/Models/User')
const Uuid = use('Uuid')

class UserController {
  /**
   * Get list of users
   *
   * GET /api/v1/users
   */
  async index({request}) {
    const query = request.get()

    const db = User.query()

    if (query.search) {
      db.where((builder) => {
        builder
          .where('fullname', 'ILIKE', `%${query.search}%`)
          .orWhere('email', 'ILIKE', `%${query.search}%`)
      })
    }

    db.orderBy('fullname', request.input('order', 'asc'))

    return db.paginate(request.input('page', 1))
  }

  /**
   * Create User
   *
   * POST /api/v1/users
   *
   * Request body:
   * {
   *   "email": "johncena@mailer.io",
   *   "fullname": "John Cena",
   *   "password": "johncenapassword123=",
   *   "role": "ADMIN"
   * }
   */
  async store({request}) {
    const user = new User()

    user.merge({
      id: Uuid.v4(),
      ...request.only(['email', 'fullname', 'password', 'role']),
    })

    await user.save()

    return user
  }

  /**
   * Get user by id
   *
   * GET /api/v1/users/:id
   */
  async show({params}) {
    return User.findOrFail(params.id)
  }

  /**
   * Update user by id
   *
   * PUT /api/v1/users/:id
   *
   * Request body:
   * {
   *   "email": "johncena@mailer.io",
   *   "fullname": "John Cena",
   *   "password": "johncenapassword123=",
   *   "role": "ADMIN"
   * }
   */
  async update({params, request}) {
    const user = await User.findOrFail(params.id)

    user.merge({
      ...request.only(['email', 'fullname', 'role']),
    })

    const password = request.input('password', null)

    if (password) {
      user.password = password
    }

    await user.save()

    return user
  }

  /**
   * Delete user by id
   *
   * DELETE /api/v1/users/:id
   */
  async destroy({params}) {
    const user = await User.findOrFail(params.id)

    await user.delete()
  }
}

module.exports = UserController
