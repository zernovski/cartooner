'use strict'

const Channel = use('App/Models/Channel')
const Uuid = use('Uuid')
const Youtube = use('Youtube')
const kue = use('Kue')
const Job = use('App/Jobs/ChannelDownload')

class ChannelController {

  /**
   * Get list of channels
   *
   * GET /api/v1/channels
   */
  async index({request}) {
    const query = request.get()

    const db = Channel.query()

    if (query.search) {
      db.where((builder) => {
        builder
          .where('name', 'ILIKE', `%${query.search}%`)
      })
    }

    db.orderBy('created_at', request.input('order', 'desc'))

    return db.paginate(request.input('page', 1))
  }

  /**
   * Create User
   *
   * POST /api/v1/channels
   *
   * Request body:
   * {
   *   "youtube_id": "johncena@mailer.io"
   * }
   */
  async store({ request, response }) {
    let data

    try {
      data = await Youtube.getChannel(request.input(['youtube_url']))
    } catch (e) {
      return response.badRequest('Unable to fetch channel data')
    }

    const channel = new Channel()

    channel.id = Uuid.v4()
    channel.youtube_id = data.id
    channel.name = data.title
    channel.thumbnail = data.thumbnails.default.url
    channel.meta = data.raw.snippet

    await channel.save()

    kue.dispatch(Job.key, {
      id: channel.id,
    })

    return channel
  }

  /**
   * Get User by Id
   *
   * GET /api/v1/channels/:id
   */
  async show({params}) {
    return Channel.findOrFail(params.id)
  }

  /**
   * Delete user by id
   *
   * DELETE /api/v1/channels/:id
   */
  async destroy({params}) {
    const channel = await Channel.findOrFail(params.id)

    await channel.delete()
  }
}

module.exports = ChannelController
