'use strict'

const Youtube = use('Youtube')

class YoutubeController {

  /**
   * Get list of channels
   *
   * GET /api/v1/youtube/channel/:channelId
   */
  async channelInfo({ params }) {
    return Youtube.getChannel(params.channelId)
  }
}

module.exports = YoutubeController
