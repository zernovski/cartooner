'use strict'

const Video = use('App/Models/Video')
const Uuid = use('Uuid')

class VideoController {

  /**
   * Get list of videos
   *
   * GET /api/v1/videos
   */
  async index({request}) {
    const query = request.get()

    const db = Video.query()

    if (query.search) {
      db.where((builder) => {
        builder
          .where('name', 'ILIKE', `%${query.search}%`)
      })
    }

    if (query.channel_id) {
      db.where('channel_id', query.channel_id)
    }

    db.orderBy('created_at', request.input('order', 'desc'))

    db.limit(20)

    if (query.offset) {
      db.offset(query.offset)
    }

    return db.fetch()
  }

  /**
   * Create User
   *
   * POST /api/v1/videos
   *
   * Request body:
   * {
   *   "youtube_id": "johncena@mailer.io"
   * }
   */
  async store({request}) {
    const video = new Video()

    video.merge({
      id: Uuid.v4(),
      ...request.only(['youtube_id']),
    })

    await video.save()

    return video
  }

  /**
   * Get User by Id
   *
   * GET /api/v1/videos/:id
   */
  async show({params}) {
    return Video.findOrFail(params.id)
  }

  /**
   * Delete user by id
   *
   * DELETE /api/v1/videos/:id
   */
  async destroy({params}) {
    const video = await Video.findOrFail(params.id)

    await video.delete()
  }
}

module.exports = VideoController
