/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/**
 * @property {string} id
 * @property {string} youtube_id
 * @property {string} name
 * @property {Object} meta
 * @property {string} thumbnail
 * @property {string} channel_id
 * @property {string} owner_id
 * @property {Date}   created_at
 * @property {Date}   updated_at
 */
class Video extends Model {
  channel() {
    return this.belongsTo('App/Models/Channel')
  }

  owner() {
    return this.belongsTo('App/Models/User')
  }
}

/*
{
  "publishedAt": "2020-05-04T14:00:09Z",
  "channelId": "UC6SyiVWGViNSKF5-UOAuQ5w",
  "title": "Finny&#39;s First Sleepover | Finny The Shark | Cartoon For Kids",
  "description": "More great Super Simple videos in the Super Simple App for iOS ▻ http://apple.co/2nW5hPd It's Finny's first sleepover and Finny is soooo nervous. Can the ...",
  "thumbnails": {
    "default": {
      "url": "https://i.ytimg.com/vi/MzT523csbr8/default.jpg",
      "width": 120,
      "height": 90
    },
    "medium": {
      "url": "https://i.ytimg.com/vi/MzT523csbr8/mqdefault.jpg",
      "width": 320,
      "height": 180
    },
    "high": {
      "url": "https://i.ytimg.com/vi/MzT523csbr8/hqdefault.jpg",
      "width": 480,
      "height": 360
    }
  },
  "channelTitle": "Finny The Shark",
  "liveBroadcastContent": "none",
  "publishTime": "2020-05-04T14:00:09Z"
}
*/

module.exports = Video
