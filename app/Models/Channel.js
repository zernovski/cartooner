'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

/**
 * @property {String}  id
 * @property {String}  youtube_id
 * @property {String}  name
 * @property {Object}  meta
 * @property {String}  thumbnail
 * @property {Date}    last_fetched_at
 * @property {String}  owner_id
 * @property {Date}    created_at
 * @property {Date}    updated_at
 */
class Channel extends Model {
  videos() {
    return this.hasMany('App/Models/Video')
  }

  owner() {
    return this.belongsTo('App/Models/User')
  }
}

/*
{
  "kind": "youtube#channel",
  "etag": "ow87tJN_M3b1WdJtb3jpuZBPlOs",
  "id": "UC6SyiVWGViNSKF5-UOAuQ5w",
  "snippet": {
    "title": "Finny The Shark",
    "description": "Follow Finny and friends as they explore stories of friendship, family, and everyday life through the (fish-eye) lens of a preschooler. Whether it’s traversing the first day of school, a trip to the dentist, or trying to be brave at a first sleepover, Finny’s wild imagination turns everyday experiences into over the top adventures! Mama Shark, Papa Shark, the pet jellyfish Sparky, and Finny’s best friends Myrtle the Turtle, Chloe the Crab, and Sammy the Seahorse are all happy to be along for the ride.",
    "publishedAt": "2019-11-25T21:32:15Z",
    "thumbnails": {
      "default": {
        "url": "https://yt3.ggpht.com/a/AATXAJw5Au25gYmrJlMqcz38EJxtjCS1F6e0-BlBdg=s88-c-k-c0xffffffff-no-rj-mo",
        "width": 88,
        "height": 88
      },
      "medium": {
        "url": "https://yt3.ggpht.com/a/AATXAJw5Au25gYmrJlMqcz38EJxtjCS1F6e0-BlBdg=s240-c-k-c0xffffffff-no-rj-mo",
        "width": 240,
        "height": 240
      },
      "high": {
        "url": "https://yt3.ggpht.com/a/AATXAJw5Au25gYmrJlMqcz38EJxtjCS1F6e0-BlBdg=s800-c-k-c0xffffffff-no-rj-mo",
        "width": 800,
        "height": 800
      }
    },
    "localized": {
      "title": "Finny The Shark",
      "description": "Follow Finny and friends as they explore stories of friendship, family, and everyday life through the (fish-eye) lens of a preschooler. Whether it’s traversing the first day of school, a trip to the dentist, or trying to be brave at a first sleepover, Finny’s wild imagination turns everyday experiences into over the top adventures! Mama Shark, Papa Shark, the pet jellyfish Sparky, and Finny’s best friends Myrtle the Turtle, Chloe the Crab, and Sammy the Seahorse are all happy to be along for the ride."
    },
    "country": "CA"
  }
}
*/

module.exports = Channel
