const webpack = require('webpack')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const WebpackNotifierPlugin = require('webpack-notifier')
const eslintFriendlyFormatter = require('eslint-friendly-formatter')
// const CleanWebpackPlugin = require('clean-webpack-plugin')
// const OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')
const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const WebpackPwaManifest = require('webpack-pwa-manifest')
const WorkboxPlugin = require('workbox-webpack-plugin')
const NODE_ENV = process.env.NODE_ENV || 'development'
const isDev = NODE_ENV !== 'production'
// const isProd = NODE_ENV === 'production'

const plugins = [
  // new CleanWebpackPlugin(),
  new VueLoaderPlugin(),
  new HtmlWebpackPlugin({
    filename: 'index.html',
    template: 'index.html',
    inject: true,
    minify: {
      collapseWhitespace: true,
      collapseInlineTagWhitespace: true,
      removeComments: true,
      removeRedundantAttributes: true,
    },
  }),
  new MiniCssExtractPlugin({
    // Options similar to the same options in webpackOptions.output
    // both options are optional
    filename: '[name].css',
    chunkFilename: '[id].css',
  }),
  new WebpackPwaManifest({
    name: 'Youtube Cartooner',
    short_name: 'yt cartooner',
    background_color: '#01579b',
    theme_color: '#01579b',
    'theme-color': '#01579b',
    start_url: '/app/',
    icons: [
      {
        src: path.resolve('ui/media/yt.png'),
        sizes: [96, 128, 192, 256, 384, 512],
        destination: path.join('assets', 'icons'),
      },
    ],
  }),
  // new MiniCssExtractPlugin(),
  new WebpackNotifierPlugin({ title: 'Webpack', contentImage: path.join(__dirname, 'ui/logo-webpack.png') }),
  // keep module.id stable when vender modules does not change
  new webpack.HashedModuleIdsPlugin(),
  // split vendor js into its own file
  // extract webpack runtime and module manifest to its own file in order to
  // prevent vendor hash from being updated whenever app bundle is updated
  new WorkboxPlugin.GenerateSW({
    clientsClaim: true,
    skipWaiting: true,
  }),
]

// if (isProd) {
//   // Compress extracted CSS. We are using this plugin so that possible
//   // duplicated CSS from different components can be deduped.
//   plugins.push(
//     new OptimizeCSSPlugin({
//       cssProcessorOptions: {
//         safe: true,
//       },
//     })
//   )
// }

module.exports = {
  mode: NODE_ENV,
  context: `${__dirname}/ui`,
  entry: {
    app: './main.js',
  },
  output: {
    path: `${__dirname}/public/app`,
    filename: '[name].mvx.js',
    chunkFilename: '[id].mvx.js',
    publicPath: '/app',
  },
  resolve: {
    extensions: ['.js', '.vue', '.json'],
    alias: {
      vue$: 'vue/dist/vue.esm.js',
      '@': `${__dirname}/ui`,
    },
  },
  optimization: {
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
    },
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          transformToRequire: {
            video: 'src',
            source: 'src',
            img: 'src',
            image: 'xlink:href',
          },
        },
      },
      {
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        options: {
          formatter: eslintFriendlyFormatter,
        },
      },
      // this will apply to both plain `.js` files
      // AND `<script>` blocks in `.vue` files
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: file => (
          /node_modules/.test(file) &&
            !/\.vue\.js/.test(file)
        ),
      },
      // this will apply to both plain `.css` files
      // AND `<style>` blocks in `.vue` files
      {
        test: /\.css$/,
        use: [
          isDev ? 'vue-style-loader' : MiniCssExtractPlugin.loader,
          { loader: 'css-loader', options: { sourceMap: isDev } },
        ],
      },
      {
        test: /\.styl(us)?$/,
        use: [
          'vue-style-loader',
          { loader: 'css-loader', options: { sourceMap: isDev } },
          { loader: 'stylus-loader', options: { sourceMap: isDev } },
        ],
      },
      {
        test: /\.(png|jpg|gif|svg|woff2?|eot|ttf|otf)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 10000,
              name: 'media/[name].[hash:7].[ext]',
            },
          },
        ],
      },
    ],
  },
  plugins,
  stats: 'minimal',
}
