const { ServiceProvider } = require('@adonisjs/fold')

class YoutubeProvider extends ServiceProvider {
  register() {
    const Config = this.app.use('Adonis/Src/Config')

    const { apiKey } = Config.get('youtube')

    this.app.singleton('Youtube', () => {
      return new (require('simple-youtube-api'))(apiKey)
    })
  }
}

module.exports = YoutubeProvider
