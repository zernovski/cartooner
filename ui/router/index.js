import Vue from 'vue'
import Router from 'vue-router'
import Authentication from '@/pages/authentication'
import Dashboard from '@/pages/dashboard'
import Users from '@/pages/users'
import Channels from '@/pages/channels'
import Watch from '@/pages/watch'
import store from '@/store'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      redirect: () => {
        return { name: 'Dashboard' }
      },
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
    },
    {
      path: '/signin',
      name: 'SignIn',
      component: Authentication,
      beforeEnter: async (to, from, next) => {
        if (store.getters['security/isLoggedIn']) {
          return next({ name: 'Dashboard' })
        }
        next()
      },
    },
    {
      path: '/users',
      name: 'Users',
      component: Users,
      meta: {
        isAllowed: () => store.getters['users/canSeeList'],
      },
    },
    {
      path: '/channels',
      name: 'Channels',
      component: Channels,
      meta: {
        isAllowed: () => store.getters['channels/canSeeList'],
      },
    },
    {
      path: '/watch/:channelId',
      name: 'Watch',
      props: true,
      component: Watch,
      meta: {
        isAllowed: () => true,
      },
    },
  ],
})

router.beforeEach(async (to, from, next) => {
  if (to.meta && to.meta.isAllowed) {
    if (to.meta.isAllowed()) {
      next()
    } else {
      next({
        name: 'SignIn',
        query: { redirect: to.fullPath },
      })
    }
    return
  }

  next()
})
export default router
