// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Vuetify from 'vuetify'
import store from './store'
import VueRouter from 'vue-router'
import router from './router'
import VeeValidate from 'vee-validate'
import VueMoment from 'vue-moment'
import VuetifyToast from 'vuetify-toast-snackbar'
// import VuePlyr from 'vue-plyr'

Vue.config.productionTip = false

Vue.use(VueRouter)
Vue.use(VeeValidate)
Vue.use(VueMoment)

Vue.use(Vuetify)

Vue.use(VuetifyToast, {
  x: 'center', // default//
  y: 'top', // default
  color: 'info', // default
  icon: 'info',
  iconColor: '', // default
  classes: [
    'body-2',
  ],
  timeout: 3000, // default
  dismissable: true, // default
  multiLine: false, // default
  vertical: false, // default
  queueable: false, // default
  showClose: false, // default
  closeText: '', // default
  closeIcon: 'close', // default
  closeColor: '', // default
  slot: [], // default
  shorts: {
    custom: {
      color: 'purple',
    },
  },
  property: '$toast', // default
})

Vue.use(VeeValidate, {
  locale: 'en',
})

// Vue.use(VuePlyr, {
//   plyr: {
//     fullscreen: { enabled: false },
//   },
//   emit: ['ended'],
// })

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>',
})


if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('/app/service-worker.js')
      .then(() => {})
      .catch(() => {})
  })
}
