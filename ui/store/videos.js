import axios from 'axios'

const getters = {
  canSeeList(state, getters, rootState, rootGetters) {
    const role = rootGetters['security/getRole']
    return Role.ADMIN === role
  },
}

const actions = {
  async getList({}, pagination) {
    const { offset, channel_id } = pagination

    const { data } = await axios.get('/api/v1/videos', {
      params: { offset, channel_id },
    })

    return data
  },


}

export default {
  namespaced: true,
  actions,
  getters,
}
