import axios from 'axios'
import { pick } from 'lodash'
import { Role } from '@app/enum'

const state = {}

// getters
const getters = {
  canSeeList(state, getters, rootState, rootGetters) {
    const role = rootGetters['security/getRole']
    return Role.ADMIN === role
  },

  canSave(state, getters, rootState, rootGetters) {
    const role = rootGetters['security/getRole']
    return Role.ADMIN === role
  },
}

// actions
const actions = {
  async getList({}, pagination) {
    const { page } = pagination
    let order

    if (pagination.descending) {
      order = 'desc'
    } else {
      order = 'asc'
    }

    const { data } = await axios.get('/api/v1/users', {
      params: { page, order },
    })

    return data
  },

  async save({}, payload) {
    const { id } = payload

    const req = pick(payload, [
      'email',
      'fullname',
      'password',
      'role',
    ])

    let res

    if (!id) {
      res = await axios.post('/api/v1/users', req)
    } else {
      res = await axios.put(`/api/v1/users/${id}`, req)
    }

    return res.data
  },
}

// mutations
const mutations = {}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
