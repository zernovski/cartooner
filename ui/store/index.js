import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'
import security from './security'
import users from './users'
import channels from './channels'
import videos from './videos'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    security,
    users,
    channels,
    videos,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : [],
})
