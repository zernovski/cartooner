import { shallowMount } from '@vue/test-utils'
import Dashboard from '../dashboard'

describe('dashboard.vue', () => {
  it('увеличивает счётчик по нажатию кнопки', () => {
    const wrapper = shallowMount(Dashboard)
    wrapper.find('button').trigger('click')
    expect(wrapper.find('div').text()).toMatch('1')
  })
})
