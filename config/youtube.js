/** @type {import('@adonisjs/framework/src/Env')} */
const Env = use('Env')

module.exports = {
  apiKey: Env.getOrFail('YOUTUBE_API_KEY'),
}
